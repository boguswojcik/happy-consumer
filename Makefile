include github.com/msales/make/golang

run:
	go generate ./ && go run ./main.go
.PHONY: run